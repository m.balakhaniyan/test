<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use Mail;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
// use GuzzleHttp;
class UserController extends Controller
{
public $successStatus = 200;
    public function login(){
        $http = new GuzzleHttp\Client;
        $response = $http->post('127.0.0.1:8000/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => '2xD0JePNPjIQ28XYVam6cS3IPWwInPJx4y7qVzrj',
                'username' => request()->phone,
                'password' => request()->password,
                'scope' => '*',
            ],
        ]);
        return json_decode((string) $response->getBody(), true);
    }

    public function register(Request $request)
            {
                $validator = Validator::make($request->all(), [
                    'phone' => 'required|unique:users|max:11',
                    'password' => 'required',
                    'fname' => 'required',
                    'lname' => 'required',
                    'email' => 'required|email|unique:users',
                    'rep_pass' => 'required|same:password',
                    'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048',
                ]);
        if ($validator->fails()) {
                    return response()->json(['error'=>$validator->errors()], 401);
                }
                $input = $request->all();
                $input['password'] = Hash::make($input['password']);
                $user = User::create($input);
//                 $user->assignRole('good_ill');
                $success['token'] =  $user->createToken('MyApp')-> accessToken;
                $success['phone'] =  $user->phone;
                $file = $request->file('avatar');
                $imageName = request()->avatar->getClientOriginalExtension();
                $imageName = 'avatar.'.substr($imageName, strrpos($imageName, "."));
                mkdir(public_path('images').'/'.$user->id);
                request()->avatar->move(public_path('images/'.$user->id), $imageName);
                $logs = fopen(public_path() .'/logs.json','a');
                fwrite($logs,json_encode(array('id'=>$user->id,'token'=>$success['token']))."\n");
                         Mail::raw('خوش آمدید.', function ($message) {
                           $message->to('m.balakhaniyan@gmail.com', 'MoReza')->subject('سلام');
                             $message->from('mrengineer5050@gmail.com','MoRezaI');
                         });
                return response()->json($user);
            }

    public function upPhone(Request $request)
        {
            $user = Auth::user();
            if($user->hasRole('good_ill')){
                $id = $user->id;
                if(User::where('id', $id)->update(['phone' => $request->phone]))
                     return response()->json(['success'=>$request->phone]);
            }

        }
    public function upAva(Request $request){
    $validator = Validator::make($request->all(), [
                        'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048',
                    ]);
                     $imageName = request()->avatar->getClientOriginalExtension();
                                    $imageName = 'avatar.'.substr($imageName, strrpos($imageName, "."));
                                    request()->avatar->move(public_path('images/'.Auth::user()->id), $imageName);
    }
    public function loggedIn(Request $request){
        return response()->json(!!Auth::user());
    }
}
